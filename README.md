# Nothing to see here

I have no idea when I'll finish this or if I ever will.

But I wanted to get some ideas down with code sketches of how things could work
for a mail client design that provides more Safety to the end user.

## Goals

 * Networking and protocol decoding is performed in sandboxed subprocesses
 * Storage is managed in sandboxed subprocess
 * GMime-based mime-parsing happens in sandboxed subprocesses
 * UI wise I don't intend for anything particular special beyond "working"
 * Early stage will only focus on IMAPv4. Possibly Maildir if there is time.

The primary thing that will make much of this possible is putting the protocol
implementations in the gnome-mail-driver process. They will listen via D-Bus to
operations from the UI process (reacting to user changes, GSettings, etc).

You can share multiple processes per-endpoint (server) but you should try to
have a single gnome-mail-driver process per-endpoint. This helps reduce the
chance that a malicious peer could access other network credentials, contacts,
or user information.

For accessing content safely, we want the storage and decoder processes to hand
us back sealed memfds.  This helps reduce the chances that a decoder bug could
ROP us into something weird. In particular, I'd really like it of the UI process
only dealt with raw framebuffers for image content.

Because of all this, a considerable amount of D-Bus interfaces will need to be
made. And I really don't like taking the time to do that so who knows how far
this will get.

-- Christian

