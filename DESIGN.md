# Mail (Project Periscope)

## Goals

 * Strong sandboxing of network, storage, and junk/malware analyzers.
 * Modern UI without going overboard on being different.
 * Modern local storage with strong indexing.
   * Fulltext for body search.
   * GVariant based storage for safe-decoding and mmap()'ability.
   * Data passing with memfd().
   * Indexes of commonly searched data (contacts, date, flags, etc).
 * junk/malware detection happens in sandboxed subprocess with integration
   for dspam/spamd/virustotal/etc.
 * Search can be performed against local storage, driver, or both.
 * When synchronizing drvier → storage, searches should be updated to
   include the new matching items.
 * Focus in IMAPv4 initially.
 * GNOME Online Accounts integration.

### Design

#### Processes

 * User Interface (Gtk shell, composer, preferences, viewer, source, plugins)
 * Storage (Database, Search, Indexers)
 * Protocol Drivers (One per-endpoint, GMime decoding, server cursors)
 * Junk and malware detection, annotation plugins

Only the user interface can talk to all subprocesses. Other processes do not
communicate with each other.

```
+----------------+            +----------------+
| User Interface |-=======-+-----------------+ |
+----------------+         | Protocol Driver |-+
  ||     ||                +-----------------+
  ||     ||
  ||     ||=+------------------------+
  ||        | Storage, Index, Search |
  ||        +------------------------+
  ||
  ||=+--------------+
     | Junk/Malware |
     +--------------+

```

IPC of documents is done as GVariant encoded documents and are passed as sealed
memfd()s. This gives us a convenient decoding-safe format and the ability
to avoid mutation of that data.

#### Components

 * Storage
   * Query Builder, Query AST, Cursor.
   * Indexers.
   * GVariant storage in LMDB to ensure that there is always safe decoding semantics.
   * Documents are versioned for easy version migration.
   * Documents shared via sealed memfd().

 * Protocol Drivers
   * IMAPv4, possibly others.
   * Peer API via D-Bus to controllability by UI process.
   * Signals emitted when more information is needed for credentials.
     The signal should indiciate what mechanisms are supported.
   * GMime decoding happens here as to not compromise other components.
   * Documents are created in GVariant format, and passed via sealed memfd()
   * IDLE (IMAPv4) can be used indicate changes to a subscription. This can
     be used to initiate a sync.

 * Preferences

 * Junk, Malware Detection, and Ancillary Annotations
   * These are annotaters that may query external systems to annotate the
     document based on content. A malware detection may use virustotal or
     something like that with a sha256.
   * Plugins may want to do extraction here to determine somethinge interesting
     from the mail content.

 * Synchronization from Driver → Storage
   * This is a set of recursive sync cursors that look at data from the driver
     and storage. Changes are applied as we walk through the dual cursors. An
     "etag" is used for version tracking.

 * Account Manager
   * Providers to load known accounts, (G-O-A, GSettings, etc)

 * Plugins. These are not a priority, but possible.
   * UI plugins to display extra data when showing emails. (Bugzilla?)
   * Annotation plugins for malware/junk/etc.
   * Protocol support.
   * Account Providers.

 * UI Components
   * Composer
   * Message View (Conversation, etc)
   * Source View (Headers, simple body)
   * Folder List (Based on EggStackList)
   * Item List

 * Telemetry for information from components.

