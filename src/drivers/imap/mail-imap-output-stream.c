/* mail-imap-output-stream.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-imap-output-stream"

#include "mail-debug.h"

#include "mail-imap-output-stream.h"

struct _MailImapOutputStream
{
  GDataOutputStream parent_instance;
};

enum {
  PROP_0,
  N_PROPS
};

G_DEFINE_TYPE (MailImapOutputStream, mail_imap_output_stream, G_TYPE_DATA_OUTPUT_STREAM)

static GParamSpec *properties [N_PROPS];

static void
mail_imap_output_stream_finalize (GObject *object)
{
  G_OBJECT_CLASS (mail_imap_output_stream_parent_class)->finalize (object);
}

static void
mail_imap_output_stream_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  MailImapOutputStream *self = MAIL_IMAP_OUTPUT_STREAM (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_output_stream_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  MailImapOutputStream *self = MAIL_IMAP_OUTPUT_STREAM (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_output_stream_class_init (MailImapOutputStreamClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_imap_output_stream_finalize;
  object_class->get_property = mail_imap_output_stream_get_property;
  object_class->set_property = mail_imap_output_stream_set_property;
}

static void
mail_imap_output_stream_init (MailImapOutputStream *self)
{
}

MailImapOutputStream *
mail_imap_output_stream_new (GOutputStream *base_stream)
{
  g_return_val_if_fail (G_IS_OUTPUT_STREAM (base_stream), NULL);

  return g_object_new (MAIL_IMAP_TYPE_OUTPUT_STREAM,
                       "base-stream", base_stream,
                       NULL);
}

static void
mail_imap_output_stream_write_bytes_cb (MailImapOutputStream *self,
                                        GAsyncResult         *result,
                                        gpointer              user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  GBytes *bytes;
  gssize written;

  g_assert (MAIL_IMAP_IS_OUTPUT_STREAM (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  bytes = g_task_get_task_data (task);
  written = g_output_stream_write_bytes_finish (G_OUTPUT_STREAM (self), result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      MAIL_EXIT;
    }

  if (written != g_bytes_get_size (bytes))
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "Short write occurred while communicated with peer");
      MAIL_EXIT;
    }

  g_task_return_boolean (task, TRUE);

  MAIL_EXIT;
}

void
mail_imap_output_stream_write_command_async (MailImapOutputStream *self,
                                             MailImapCommand      *command,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GError) error = NULL;

  MAIL_ENTRY;

  g_return_if_fail (MAIL_IMAP_IS_OUTPUT_STREAM (self));
  g_return_if_fail (MAIL_IMAP_IS_COMMAND (command));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, mail_imap_output_stream_write_command_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  bytes = mail_imap_command_serialize (command, &error);

  if (bytes == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      MAIL_EXIT;
    }

  g_task_set_task_data (task, g_bytes_ref (bytes), (GDestroyNotify)g_bytes_unref);

  g_output_stream_write_bytes_async (G_OUTPUT_STREAM (self),
                                     bytes,
                                     G_PRIORITY_LOW,
                                     cancellable,
                                     (GAsyncReadyCallback) mail_imap_output_stream_write_bytes_cb,
                                     g_steal_pointer (&task));

  MAIL_EXIT;
}

gboolean
mail_imap_output_stream_write_command_finish (MailImapOutputStream  *self,
                                              GAsyncResult          *result,
                                              GError               **error)
{
  g_return_val_if_fail (MAIL_IMAP_IS_OUTPUT_STREAM (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
