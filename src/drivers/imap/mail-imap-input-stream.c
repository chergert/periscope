/* mail-imap-input-stream.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-imap-input-stream"

#include "mail-debug.h"

#include "mail-imap-input-stream.h"

struct _MailImapInputStream
{
  GDataInputStream parent_instance;
};

enum {
  PROP_0,
  N_PROPS
};

G_DEFINE_TYPE (MailImapInputStream, mail_imap_input_stream, G_TYPE_DATA_INPUT_STREAM)

static GParamSpec *properties [N_PROPS];

static void
mail_imap_input_stream_finalize (GObject *object)
{
  G_OBJECT_CLASS (mail_imap_input_stream_parent_class)->finalize (object);
}

static void
mail_imap_input_stream_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  MailImapInputStream *self = MAIL_IMAP_INPUT_STREAM (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_input_stream_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  MailImapInputStream *self = MAIL_IMAP_INPUT_STREAM (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_input_stream_class_init (MailImapInputStreamClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_imap_input_stream_finalize;
  object_class->get_property = mail_imap_input_stream_get_property;
  object_class->set_property = mail_imap_input_stream_set_property;
}

static void
mail_imap_input_stream_init (MailImapInputStream *self)
{
}

MailImapInputStream *
mail_imap_input_stream_new (GInputStream *base_stream)
{
  g_return_val_if_fail (G_IS_INPUT_STREAM (base_stream), NULL);

  return g_object_new (MAIL_IMAP_TYPE_INPUT_STREAM,
                       "base-stream", base_stream,
                       NULL);
}

static void
mail_imap_input_stream_read_line_cb (MailImapInputStream *self,
                                     GAsyncResult        *result,
                                     gpointer             user_data)
{
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autofree gchar *line = NULL;
  MailImapReply *reply;
  gsize len = 0;

  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_INPUT_STREAM (self));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  reply = g_task_get_task_data (task);
  g_assert (MAIL_IMAP_IS_REPLY (reply));

  line = g_data_input_stream_read_line_finish_utf8 (G_DATA_INPUT_STREAM (self),
                                                    result,
                                                    &len,
                                                    &error);

  g_print (">> %s\n", line);

  g_task_return_pointer (task, g_object_ref (reply), g_object_unref);

  MAIL_EXIT;
}

void
mail_imap_input_stream_read_reply_async (MailImapInputStream *self,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(MailImapReply) reply = NULL;

  MAIL_ENTRY;

  g_return_if_fail (MAIL_IMAP_IS_INPUT_STREAM (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, mail_imap_input_stream_read_reply_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  reply = mail_imap_reply_new ();

  g_task_set_task_data (task, g_steal_pointer (&reply), g_object_unref);

  g_data_input_stream_read_line_async (G_DATA_INPUT_STREAM (self),
                                       G_PRIORITY_LOW,
                                       cancellable,
                                       (GAsyncReadyCallback) mail_imap_input_stream_read_line_cb,
                                       g_steal_pointer (&task));

  MAIL_EXIT;
}

/**
 * mail_imap_input_stream_read_reply_finish:
 * @self: a #MailImapInputStream
 * @result: the #GAsyncResult passed to the async callback
 * @error: a location for a #GError, or %NULL
 *
 * Completes an asynchronous request to
 * mail_imap_input_stream_read_reply_async().
 *
 * Note that receiving a #MailImapReply instance from this function does not
 * mean that the command that was executed was successful. Only that we
 * received a reply from the IMAP server.
 *
 * Returns: (transfer full): A #MailImapReply if succesful, otherwise
 *   %NULL and @error is set.
 */
MailImapReply *
mail_imap_input_stream_read_reply_finish (MailImapInputStream  *self,
                                          GAsyncResult         *result,
                                          GError              **error)
{
  g_return_val_if_fail (MAIL_IMAP_IS_INPUT_STREAM (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}
