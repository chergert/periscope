/* mail-imap-client.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_IMAP_CLIENT_H
#define MAIL_IMAP_CLIENT_H

#include <gio/gio.h>

#include "mail-credentials.h"

#include "mail-imap-command.h"
#include "mail-imap-reply.h"

G_BEGIN_DECLS

#define MAIL_IMAP_TYPE_CLIENT (mail_imap_client_get_type())

G_DECLARE_FINAL_TYPE (MailImapClient, mail_imap_client, MAIL_IMAP, CLIENT, GObject)

MailImapClient *mail_imap_client_new                 (GIOStream            *base_io_stream);
void            mail_imap_client_listen_async        (MailImapClient       *self,
                                                      GCancellable         *cancellable,
                                                      GAsyncReadyCallback   callback,
                                                      gpointer              user_data);
gboolean        mail_imap_client_listen_finish       (MailImapClient       *self,
                                                      GAsyncResult         *result,
                                                      GError              **error);
void            mail_imap_client_execute_async       (MailImapClient       *self,
                                                      MailImapCommand      *command,
                                                      GCancellable         *cancellable,
                                                      GAsyncReadyCallback   callback,
                                                      gpointer              user_data);
MailImapReply  *mail_imap_client_execute_finish      (MailImapClient       *self,
                                                      GAsyncResult         *result,
                                                      GError              **error);
void            mail_imap_client_authenticate_async  (MailImapClient       *self,
                                                      MailCredentials      *credentials,
                                                      GCancellable         *cancellable,
                                                      GAsyncReadyCallback   callback,
                                                      gpointer              user_data);
gboolean        mail_imap_client_authenticate_finish (MailImapClient       *self,
                                                      GAsyncResult         *result,
                                                      GError              **error);

G_END_DECLS

#endif /* MAIL_IMAP_CLIENT_H */
