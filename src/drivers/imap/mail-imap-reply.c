/* mail-imap-reply.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-imap-reply"

#include "mail-imap-reply.h"

struct _MailImapReply
{
  GObject parent_instance;
};

enum {
  PROP_0,
  N_PROPS
};

G_DEFINE_TYPE (MailImapReply, mail_imap_reply, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static void
mail_imap_reply_finalize (GObject *object)
{
  G_OBJECT_CLASS (mail_imap_reply_parent_class)->finalize (object);
}

static void
mail_imap_reply_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  MailImapReply *self = MAIL_IMAP_REPLY (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_reply_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  MailImapReply *self = MAIL_IMAP_REPLY (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_reply_class_init (MailImapReplyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_imap_reply_finalize;
  object_class->get_property = mail_imap_reply_get_property;
  object_class->set_property = mail_imap_reply_set_property;
}

static void
mail_imap_reply_init (MailImapReply *self)
{
}

/**
 * mail_imap_reply_new:
 *
 * This function creates a new #MailImapReply.
 *
 * Generally, you should not need to use this function as they are created as
 * necessary by the #MailImapInputStream class.
 *
 * Returns: (transfer full): A #MailImapReply.
 */
MailImapReply *
mail_imap_reply_new (void)
{
  return g_object_new (MAIL_IMAP_TYPE_REPLY, NULL);
}
