/* mail-imap-output-stream.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_IMAP_OUTPUT_STREAM_H
#define MAIL_IMAP_OUTPUT_STREAM_H

#include <gio/gio.h>

#include "mail-imap-command.h"

G_BEGIN_DECLS

#define MAIL_IMAP_TYPE_OUTPUT_STREAM (mail_imap_output_stream_get_type())

G_DECLARE_FINAL_TYPE (MailImapOutputStream, mail_imap_output_stream, MAIL_IMAP, OUTPUT_STREAM, GDataOutputStream)

MailImapOutputStream *mail_imap_output_stream_new                  (GOutputStream         *base_stream);
void                  mail_imap_output_stream_write_command_async  (MailImapOutputStream  *self,
                                                                    MailImapCommand       *command,
                                                                    GCancellable          *cancellable,
                                                                    GAsyncReadyCallback    callback,
                                                                    gpointer               user_data);
gboolean              mail_imap_output_stream_write_command_finish (MailImapOutputStream  *self,
                                                                    GAsyncResult          *result,
                                                                    GError               **error);

G_END_DECLS

#endif /* MAIL_IMAP_OUTPUT_STREAM_H */
