/* mail-imap-client.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-imap-client"

#include <glib/gi18n.h>

#include "mail-debug.h"

#include "mail-imap-capability-command.h"
#include "mail-imap-client.h"
#include "mail-imap-input-stream.h"
#include "mail-imap-output-stream.h"

struct _MailImapClient
{
  GObject               parent_instance;

  GIOStream            *base_io_stream;
  MailImapInputStream  *input_stream;
  MailImapOutputStream *output_stream;
  GCancellable         *dispose_cancellable;

  GQueue                command_queue;
  guint64               command_sequence;

  guint                 has_shutdown : 1;
};

enum {
  PROP_0,
  PROP_BASE_IO_STREAM,
  N_PROPS
};

G_DEFINE_TYPE (MailImapClient, mail_imap_client, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static gchar *
mail_imap_client_sequence (MailImapClient *self)
{
  g_assert (MAIL_IMAP_IS_CLIENT (self));

  return g_strdup_printf ("%"G_GUINT64_FORMAT, ++self->command_sequence);
}

static void
mail_imap_client_panic (MailImapClient *self,
                        const GError   *error)
{
  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_CLIENT (self));
  g_assert (error != NULL);

  g_debug ("client panic due to: %s", error->message);

  self->has_shutdown = TRUE;

  /* TODO: Cancel all in flight tasks */

  MAIL_EXIT;
}

static void
mail_imap_client_command_written_cb (GObject      *object,
                                     GAsyncResult *result,
                                     gpointer      user_data)
{
  MailImapOutputStream *stream = (MailImapOutputStream *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  MailImapClient *self;

  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_OUTPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);

  g_assert (MAIL_IMAP_IS_CLIENT (self));

  if (!mail_imap_output_stream_write_command_finish (stream, result, &error))
    {
      mail_imap_client_panic (self, error);
      g_task_return_error (task, g_steal_pointer (&error));
    }

  /* Now wait for the reply */

  MAIL_EXIT;
}

static void
mail_imap_client_maybe_flush (MailImapClient *self)
{
  g_autoptr(GTask) task = NULL;

  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_CLIENT (self));
  g_assert (self->output_stream != NULL);
  g_assert (self->input_stream != NULL);

  task = g_queue_pop_head (&self->command_queue);

  if (task != NULL)
    {
      MailImapCommand *command = g_task_get_task_data (task);
      GCancellable *cancellable = g_task_get_cancellable (task);

      mail_imap_output_stream_write_command_async (self->output_stream,
                                                   command,
                                                   cancellable,
                                                   mail_imap_client_command_written_cb,
                                                   g_steal_pointer (&task));
    }

  MAIL_EXIT;
}

static gboolean
mail_imap_client_check_ready (MailImapClient *self,
                              GTask          *task)
{
  g_assert (MAIL_IMAP_IS_CLIENT (self));
  g_assert (G_IS_TASK (task));

  if (self->has_shutdown || !self->output_stream || !self->input_stream)
    {
      g_task_return_new_error (task,
                               G_IO_ERROR,
                               G_IO_ERROR_CLOSED,
                               "Cannot submit commands after connection has closed");
      return FALSE;
    }

  return TRUE;
}

static void
mail_imap_client_capability_cb (MailImapClient *self,
                                  GAsyncResult   *result,
                                  gpointer        unused)
{
  g_autoptr(MailImapReply) reply = NULL;
  g_autoptr(GError) error = NULL;

  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_CLIENT (self));
  g_assert (G_IS_ASYNC_RESULT (result));

  reply = mail_imap_client_execute_finish (self, result, &error);

  if (error != NULL)
    {
      mail_imap_client_panic (self, error);
      MAIL_EXIT;
    }

  g_assert (MAIL_IMAP_IS_REPLY (reply));

  MAIL_EXIT;
}

static void
mail_imap_client_constructed (GObject *object)
{
  MailImapClient *self = (MailImapClient *)object;
  g_autoptr(MailImapCommand) caps_command = NULL;
  GOutputStream *base_output_stream;
  GInputStream *base_input_stream;

  g_assert (MAIL_IMAP_IS_CLIENT (self));

  G_OBJECT_CLASS (mail_imap_client_parent_class)->constructed (object);

  if (self->base_io_stream == NULL)
    {
      g_warning ("%s created without an underlying stream.",
                 G_OBJECT_TYPE_NAME (self));
      return;
    }

  base_output_stream = g_io_stream_get_output_stream (self->base_io_stream);
  base_input_stream = g_io_stream_get_input_stream (self->base_io_stream);

  g_return_if_fail (G_IS_OUTPUT_STREAM (base_output_stream));
  g_return_if_fail (G_IS_INPUT_STREAM (base_input_stream));

  self->output_stream = mail_imap_output_stream_new (base_output_stream);
  self->input_stream = mail_imap_input_stream_new (base_input_stream);

  g_return_if_fail (MAIL_IMAP_IS_OUTPUT_STREAM (self->output_stream));
  g_return_if_fail (MAIL_IMAP_IS_INPUT_STREAM (self->input_stream));

  /* We immediately queue a capability negotiation since we will always
   * need that for future commands.
   */
  caps_command = mail_imap_capability_command_new ();
  mail_imap_client_execute_async (self,
                                  caps_command,
                                  self->dispose_cancellable,
                                  (GAsyncReadyCallback) mail_imap_client_capability_cb,
                                  NULL);
}

static void
mail_imap_client_dispose (GObject *object)
{
  MailImapClient *self = (MailImapClient *)object;

  g_assert (MAIL_IMAP_IS_CLIENT (self));

  if (!g_cancellable_is_cancelled (self->dispose_cancellable))
    g_cancellable_cancel (self->dispose_cancellable);

  G_OBJECT_CLASS (mail_imap_client_parent_class)->dispose (object);
}

static void
mail_imap_client_finalize (GObject *object)
{
  MailImapClient *self = (MailImapClient *)object;

  g_clear_object (&self->output_stream);
  g_clear_object (&self->input_stream);
  g_clear_object (&self->base_io_stream);
  g_clear_object (&self->dispose_cancellable);

  G_OBJECT_CLASS (mail_imap_client_parent_class)->finalize (object);
}

static void
mail_imap_client_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  MailImapClient *self = MAIL_IMAP_CLIENT (object);

  switch (prop_id)
    {
    case PROP_BASE_IO_STREAM:
      g_value_set_object (value, self->base_io_stream);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_client_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  MailImapClient *self = MAIL_IMAP_CLIENT (object);

  switch (prop_id)
    {
    case PROP_BASE_IO_STREAM:
      self->base_io_stream = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_client_class_init (MailImapClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = mail_imap_client_constructed;
  object_class->dispose = mail_imap_client_dispose;
  object_class->finalize = mail_imap_client_finalize;
  object_class->get_property = mail_imap_client_get_property;
  object_class->set_property = mail_imap_client_set_property;

  properties [PROP_BASE_IO_STREAM] =
    g_param_spec_object ("base-io-stream",
                         "Base IO Stream",
                         "The underlying stream to communicate over",
                         G_TYPE_IO_STREAM,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
mail_imap_client_init (MailImapClient *self)
{
  self->dispose_cancellable = g_cancellable_new ();
}

MailImapClient *
mail_imap_client_new (GIOStream *base_stream)
{
  return g_object_new (MAIL_IMAP_TYPE_CLIENT,
                       "base-io-stream", base_stream,
                       NULL);
}

void
mail_imap_client_authenticate_async (MailImapClient      *self,
                                     MailCredentials     *credentials,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (MAIL_IMAP_IS_CLIENT (self));
  g_return_if_fail (!credentials || MAIL_IS_CREDENTIALS (credentials));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, mail_imap_client_authenticate_async);
  g_task_set_priority (task, G_PRIORITY_LOW);

  g_task_return_boolean (task, TRUE);
}

gboolean
mail_imap_client_authenticate_finish (MailImapClient  *self,
                                      GAsyncResult    *result,
                                      GError         **error)
{
  g_return_val_if_fail (MAIL_IMAP_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
mail_imap_client_execute_async (MailImapClient      *self,
                                MailImapCommand     *command,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autofree gchar *sequence = NULL;

  MAIL_ENTRY;

  g_return_if_fail (MAIL_IMAP_IS_CLIENT (self));
  g_return_if_fail (MAIL_IMAP_IS_COMMAND (command));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, mail_imap_client_execute_async);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_set_task_data (task, g_object_ref (command), g_object_unref);

  sequence = mail_imap_client_sequence (self);
  mail_imap_command_set_id (command, sequence);

  if (!mail_imap_client_check_ready (self, task))
    MAIL_EXIT;

  g_queue_push_tail (&self->command_queue, g_steal_pointer (&task));

  mail_imap_client_maybe_flush (self);

  MAIL_EXIT;
}

/**
 * mail_imap_client_execute_finish:
 * @self: a #MailImapClient
 * @result: A #GAsyncResult
 * @error: A location for a #GError, or %NULL
 *
 * This completes the asynchronous operation to execute a command on the
 * IMAP server. Getting a #MailImapReply from this function does not indicate
 * that the operation completed successfully, only that the command was
 * executed and a reply was received.
 *
 * If you receive %NULL from this method, it is possible the command was
 * executed but a network parition occurred or the operation was cancelled
 * on the client side.
 *
 * Returns: (transfer full): A #MailImapReply if successful; otherwise
 *   %NULL and @error is set.
 */
MailImapReply *
mail_imap_client_execute_finish (MailImapClient  *self,
                                 GAsyncResult    *result,
                                 GError         **error)
{
  g_return_val_if_fail (MAIL_IMAP_IS_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
mail_imap_client_dispatch (MailImapClient *self,
                           MailImapReply  *reply)
{
  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_CLIENT (self));
  g_assert (MAIL_IMAP_IS_REPLY (reply));

  // TODO: dispatch reply

  MAIL_EXIT;
}

static void
mail_imap_client_read_reply_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  MailImapInputStream *stream = (MailImapInputStream *)object;
  g_autoptr(MailImapClient) self = NULL;
  g_autoptr(MailImapReply) reply = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;

  MAIL_ENTRY;

  g_assert (MAIL_IMAP_IS_INPUT_STREAM (stream));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  /* Take a full reference to self so we can release task */
  self = MAIL_IMAP_CLIENT (g_async_result_get_source_object (G_ASYNC_RESULT (task)));
  g_assert (MAIL_IMAP_IS_CLIENT (self));

  reply = mail_imap_input_stream_read_reply_finish (stream, result, &error);

  g_assert (!reply || MAIL_IMAP_IS_REPLY (reply));
  g_assert (reply || error);

  if (reply == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      MAIL_EXIT;
    }

  /*
   * We begin the next read before dispatching the reply for two reasons.
   * Primarily, it preserves ordering so that if the reply callback closes the
   * client using mail_imap_client_shutdown_async(), we don't have to deal with
   * weird ordering here. Also, it allows the socket to make forward progress
   * during our callback.
   */

  mail_imap_input_stream_read_reply_async (self->input_stream,
                                           self->dispose_cancellable,
                                           mail_imap_client_read_reply_cb,
                                           g_steal_pointer (&task));

  mail_imap_client_dispatch (self, reply);

  MAIL_EXIT;
}

/**
 * mail_imap_client_listen_async:
 * @self: a #MailImapClient.
 * @cancellable: (nullable): an optional #GCancellable, or %NULL.
 * @callback: (scope async) (closure user_data): a callback to execute upon
 *   completion.
 * @user_data: the data to pass to @callback function.
 *
 * Starts listening to the IMAP server and processing replies.
 *
 * It is the client consumers responsibility to call this function to begin
 * processing data from the IMAP server.
 *
 * Call mail_imap_client_listen_finish() from @callback to complete the
 * operation.
 *
 * Call mail_imap_client_shutdown_async() to stop processing messages.
 */
void
mail_imap_client_listen_async (MailImapClient      *self,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  MAIL_ENTRY;

  g_return_if_fail (MAIL_IMAP_IS_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, mail_imap_client_listen_async);
  g_task_set_priority (task, G_PRIORITY_LOW);
  g_task_set_return_on_cancel (task, TRUE);

  if (!mail_imap_client_check_ready (self, task))
    MAIL_EXIT;

  mail_imap_input_stream_read_reply_async (self->input_stream,
                                           self->dispose_cancellable,
                                           mail_imap_client_read_reply_cb,
                                           g_steal_pointer (&task));

  MAIL_EXIT;
}

/**
 * mail_imap_client_listen_finish:
 * @self: a #MailImapClient
 *
 * Completes a call to mail_imap_client_listen_async().
 *
 * This should be called by the callback provided to
 * mail_imap_client_listen_async() to get the error associated with the reply
 * procssing loop.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
mail_imap_client_listen_finish (MailImapClient  *self,
                                GAsyncResult    *result,
                                GError         **error)
{
  gboolean ret;

  MAIL_ENTRY;

  g_return_val_if_fail (MAIL_IMAP_IS_CLIENT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  ret = g_task_propagate_boolean (G_TASK (result), error);

  MAIL_RETURN (ret);
}
