/* mail-imap-command.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_IMAP_COMMAND_H
#define MAIL_IMAP_COMMAND_H

#include <gio/gio.h>

G_BEGIN_DECLS

#define MAIL_IMAP_TYPE_COMMAND (mail_imap_command_get_type())

G_DECLARE_DERIVABLE_TYPE (MailImapCommand, mail_imap_command, MAIL_IMAP, COMMAND, GObject)

struct _MailImapCommandClass
{
  GObjectClass parent_class;

  GBytes *(*serialize) (MailImapCommand  *self,
                        GError          **error);

  gpointer _reserved1;
  gpointer _reserved2;
  gpointer _reserved3;
  gpointer _reserved4;
  gpointer _reserved5;
  gpointer _reserved6;
  gpointer _reserved7;
  gpointer _reserved8;
};

const gchar *mail_imap_command_get_id       (MailImapCommand  *self);
void         mail_imap_command_set_id       (MailImapCommand  *self,
                                             const gchar      *id);
GBytes      *mail_imap_command_serialize    (MailImapCommand  *self,
                                             GError          **error);
GBytes      *mail_imap_command_build_simple (MailImapCommand  *self,
                                             const gchar      *command,
                                             GError          **error);

G_END_DECLS

#endif /* MAIL_IMAP_COMMAND_H */
