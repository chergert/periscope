/* mail-imap-capability-command.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-imap-capability-command"

#include "mail-imap-capability-command.h"

/**
 * SECTION:mail-imap-capability-command
 * @title: MailImapCapabilitiesCommand
 * @short_description: IMAP capability negotiation
 *
 * This #MailImapCommand is used to perform capability negotiation at the
 * beginning of an established connection. The result from this command should
 * be used to determine what features the peer supports so the client can
 * act accordingly.
 *
 * The #MailImapClient will automatically submit this command upon the
 * initialization of the connection.
 */

struct _MailImapCapabilitiesCommand
{
  MailImapCommand parent_instance;
};

G_DEFINE_TYPE (MailImapCapabilitiesCommand, mail_imap_capability_command, MAIL_IMAP_TYPE_COMMAND)

static GBytes *
mail_imap_capability_command_serialize (MailImapCommand  *command,
                                          GError          **error)
{
  return mail_imap_command_build_simple (command, "CAPABILITY", error);
}

static void
mail_imap_capability_command_class_init (MailImapCapabilitiesCommandClass *klass)
{
  MailImapCommandClass *command_class = MAIL_IMAP_COMMAND_CLASS (klass);

  command_class->serialize = mail_imap_capability_command_serialize;
}

static void
mail_imap_capability_command_init (MailImapCapabilitiesCommand *self)
{
}

MailImapCommand *
mail_imap_capability_command_new (void)
{
  return g_object_new (MAIL_IMAP_TYPE_CAPABILITY_COMMAND, NULL);
}
