/* mail-imap-command.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-imap-command"

#include <string.h>

#include "mail-imap-command.h"

typedef struct
{
  gchar *id;
} MailImapCommandPrivate;

enum {
  PROP_0,
  PROP_ID,
  N_PROPS
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (MailImapCommand, mail_imap_command, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static GBytes *
mail_imap_command_real_serialize (MailImapCommand  *self,
                                  GError          **error)
{
  g_set_error (error,
               G_IO_ERROR,
               G_IO_ERROR_NOT_SUPPORTED,
               "%s failed to implement serialize",
               G_OBJECT_TYPE_NAME (self));
  return NULL;
}

static void
mail_imap_command_finalize (GObject *object)
{
  MailImapCommand *self = (MailImapCommand *)object;
  MailImapCommandPrivate *priv = mail_imap_command_get_instance_private (self);

  g_clear_pointer (&priv->id, g_free);

  G_OBJECT_CLASS (mail_imap_command_parent_class)->finalize (object);
}

static void
mail_imap_command_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  MailImapCommand *self = MAIL_IMAP_COMMAND (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, mail_imap_command_get_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_command_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  MailImapCommand *self = MAIL_IMAP_COMMAND (object);

  switch (prop_id)
    {
    case PROP_ID:
      mail_imap_command_set_id (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_imap_command_class_init (MailImapCommandClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_imap_command_finalize;
  object_class->get_property = mail_imap_command_get_property;
  object_class->set_property = mail_imap_command_set_property;

  klass->serialize = mail_imap_command_real_serialize;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The unique command identifier",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
mail_imap_command_init (MailImapCommand *self)
{
}

const gchar *
mail_imap_command_get_id (MailImapCommand *self)
{
  MailImapCommandPrivate *priv = mail_imap_command_get_instance_private (self);

  g_return_val_if_fail (MAIL_IMAP_IS_COMMAND (self), NULL);

  return priv->id;
}

void
mail_imap_command_set_id (MailImapCommand *self,
                          const gchar     *id)
{
  MailImapCommandPrivate *priv = mail_imap_command_get_instance_private (self);

  g_return_if_fail (MAIL_IMAP_IS_COMMAND (self));

  if (g_strcmp0 (id, priv->id) != 0)
    {
      g_free (priv->id);
      priv->id = g_strdup (id);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ID]);
    }
}

GBytes *
mail_imap_command_serialize (MailImapCommand  *self,
                             GError          **error)
{
  g_return_val_if_fail (MAIL_IMAP_IS_COMMAND (self), NULL);

  return MAIL_IMAP_COMMAND_GET_CLASS (self)->serialize (self, error);
}

GBytes *
mail_imap_command_build_simple (MailImapCommand  *self,
                                const gchar      *command,
                                GError          **error)
{
  MailImapCommandPrivate *priv = mail_imap_command_get_instance_private (self);
  gchar *str;
  gsize len;

  g_return_val_if_fail (MAIL_IMAP_IS_COMMAND (self), NULL);
  g_return_val_if_fail (command != NULL, NULL);

  if (priv->id == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_INVAL,
                   "Cannot call %s() before an id has been set",
                   G_STRFUNC);
      return NULL;
    }

  for (const gchar *iter = command; *iter; iter = g_utf8_next_char (iter))
    {
      if (g_unichar_isalnum (g_utf8_get_char (iter)))
        continue;

      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_INVAL,
                   "Command %s contains invalid characters",
                   command);
      return NULL;
    }

  str = g_strdup_printf ("%s %s\n", priv->id, command);
  len = strlen (str);

  return g_bytes_new_take (str, len);
}
