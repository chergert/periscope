/* gnome-mail-driver.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "gnome-mail-driver"

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <glib/gi18n.h>
#include <gio/gio.h>

#include "gnome-mail-driver.h"

static const GOptionEntry option_entries[] = {
  { "protocol", 'p', 0, G_OPTION_ARG_STRING, NULL,
    N_("The protocol to use for communication"),
    N_("PROTOCOL") },

  { NULL }
};

struct _MailDriver
{
  GApplication         parent_instance;

  gchar               *protocol_name;
  MailProtocolService *service;
};

G_DEFINE_TYPE (MailDriver, mail_driver, G_TYPE_APPLICATION)

static gboolean
mail_driver_create_service (MailDriver *self,
                            GError     **error)
{
  MailProtocolType type = 0;

  MAIL_ENTRY;

  g_assert (MAIL_IS_DRIVER (self));

  type = mail_protocol_type_from_string (self->protocol_name);

  if (type == 0)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Unknown protocol type");
      MAIL_RETURN (FALSE);
    }

  self->service = g_object_new (MAIL_TYPE_PROTOCOL_SERVICE,
                                "protocol", type,
                                NULL);

  MAIL_RETURN (TRUE);
}

static void
mail_driver_activate (GApplication *app)
{
  MailDriver *self = (MailDriver *)app;

  MAIL_ENTRY;

  g_assert (MAIL_IS_DRIVER (self));

  if (self->service != NULL)
    {
      /* TODO: Start listening */
      g_application_hold (G_APPLICATION (self));
    }

  MAIL_EXIT;
}

static gint
mail_driver_handle_local_options (GApplication *app,
                                  GVariantDict *options)
{
  MailDriver *self = (MailDriver *)app;

  MAIL_ENTRY;

  g_assert (MAIL_IS_DRIVER (self));
  g_assert (options != NULL);

  g_clear_pointer (&self->protocol_name, g_free);

  if (g_variant_dict_contains (options, "protocol"))
    g_variant_dict_lookup (options, "protocol", "s", &self->protocol_name);

  MAIL_RETURN (-1);
}

static void
mail_driver_startup (GApplication *app)
{
  MailDriver *self = (MailDriver *)app;
  MailProtocolType type = 0;

  g_assert (MAIL_IS_DRIVER (self));

  G_APPLICATION_CLASS (mail_driver_parent_class)->startup (app);

  type = mail_protocol_type_from_string (self->protocol_name);

  if (type != 0)
    self->service = g_object_new (MAIL_TYPE_PROTOCOL_SERVICE,
                                  "protocol", type,
                                  NULL);
}

static gboolean
mail_driver_dbus_register (GApplication     *app,
                           GDBusConnection  *connection,
                           const gchar      *object_path,
                           GError          **error)
{
  MailDriver *self = (MailDriver *)app;

  MAIL_ENTRY;

  g_assert (MAIL_IS_DRIVER (self));
  g_assert (G_IS_DBUS_CONNECTION (connection));
  g_assert (object_path != NULL);

  if (!G_APPLICATION_CLASS (mail_driver_parent_class)->dbus_register (app, connection, object_path, error))
    MAIL_RETURN (FALSE);

  /* TODO Register service */

  MAIL_RETURN (TRUE);
}

static void
mail_driver_class_init (MailDriverClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = mail_driver_activate;
  app_class->startup = mail_driver_startup;
  app_class->dbus_register = mail_driver_dbus_register;
  app_class->handle_local_options = mail_driver_handle_local_options;
}

static void
mail_driver_init (MailDriver *self)
{
  g_application_add_main_option_entries (G_APPLICATION (self), option_entries);
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr(GApplication) app = NULL;
  gint ret;

  app = g_object_new (MAIL_TYPE_DRIVER,
                      "application-id", "org.gnome.Mail.Driver",
                      "flags", G_APPLICATION_NON_UNIQUE,
                      NULL);


  ret = g_application_run (app, argc, argv);

  return ret;
}
