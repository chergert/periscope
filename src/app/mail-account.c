/* mail-account.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-account"

#include "mail-account.h"

enum {
  PROP_0,
  PROP_ID,
  PROP_TITLE,
  N_PROPS
};

G_DEFINE_ABSTRACT_TYPE (MailAccount, mail_account, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static void
mail_account_real_delete_async (MailAccount         *self,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  g_assert (MAIL_IS_ACCOUNT (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  g_task_report_new_error (self, callback, user_data,
                           mail_account_real_delete_async,
                           G_IO_ERROR,
                           G_IO_ERROR_NOT_SUPPORTED,
                           "%s does not support deleting accounts",
                           G_OBJECT_TYPE_NAME (self));

}

static gboolean
mail_account_real_delete_finish (MailAccount   *self,
                                 GAsyncResult  *result,
                                 GError       **error)
{
  g_assert (MAIL_IS_ACCOUNT (self));
  g_assert (G_IS_TASK (result));

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
mail_account_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  MailAccount *self = MAIL_ACCOUNT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, mail_account_get_id (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, mail_account_get_title (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_account_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  MailAccount *self = MAIL_ACCOUNT (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      mail_account_set_title (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_account_class_init (MailAccountClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = mail_account_get_property;
  object_class->set_property = mail_account_set_property;

  klass->delete_async = mail_account_real_delete_async;
  klass->delete_finish = mail_account_real_delete_finish;

  properties [PROP_ID] =
    g_param_spec_string ("id",
                         "Id",
                         "The identifier for the account",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "The title for the account",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
mail_account_init (MailAccount *self)
{
}

MailAccount *
mail_account_new (void)
{
  return g_object_new (MAIL_TYPE_ACCOUNT, NULL);
}

const gchar *
mail_account_get_id (MailAccount *self)
{
  g_return_val_if_fail (MAIL_IS_ACCOUNT (self), NULL);

  return MAIL_ACCOUNT_GET_CLASS (self)->get_id (self);
}

const gchar *
mail_account_get_title (MailAccount *self)
{
  g_return_val_if_fail (MAIL_IS_ACCOUNT (self), NULL);

  return MAIL_ACCOUNT_GET_CLASS (self)->get_title (self);
}

void
mail_account_set_title (MailAccount *self,
                        const gchar *title)
{
  g_return_if_fail (MAIL_IS_ACCOUNT (self));

  MAIL_ACCOUNT_GET_CLASS (self)->set_title (self, title);
}

void
mail_account_delete_async (MailAccount         *self,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data)
{
  g_return_if_fail (MAIL_IS_ACCOUNT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  MAIL_ACCOUNT_GET_CLASS (self)->delete_async (self, cancellable, callback, user_data);
}

gboolean
mail_account_delete_finish (MailAccount   *self,
                            GAsyncResult  *result,
                            GError       **error)
{
  g_return_val_if_fail (MAIL_IS_ACCOUNT (self), FALSE);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), FALSE);

  MAIL_ACCOUNT_GET_CLASS (self)->delete_finish (self, result, error);
}
