/* mail-window.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-window"

#include "mail-window.h"

struct _MailWindow
{
  GtkApplicationWindow parent_instance;
};

enum {
  PROP_0,
  N_PROPS
};

G_DEFINE_TYPE (MailWindow, mail_window, GTK_TYPE_APPLICATION_WINDOW)

static GParamSpec *properties [N_PROPS];

static void
mail_window_finalize (GObject *object)
{
  G_OBJECT_CLASS (mail_window_parent_class)->finalize (object);
}

static void
mail_window_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  MailWindow *self = MAIL_WINDOW (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_window_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  MailWindow *self = MAIL_WINDOW (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_window_class_init (MailWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_window_finalize;
  object_class->get_property = mail_window_get_property;
  object_class->set_property = mail_window_set_property;
}

static void
mail_window_init (MailWindow *self)
{
}
