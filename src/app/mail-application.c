/* mail-application.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-application"

#include "mail-debug.h"

#include "mail-application.h"
#include "mail-window.h"

struct _MailApplication
{
  GtkApplication      parent_instance;
  MailAccountManager *accounts;
};

G_DEFINE_TYPE (MailApplication, mail_application, GTK_TYPE_APPLICATION)

static void
mail_application_activate (GApplication *app)
{
  MailApplication *self = (MailApplication *)app;
  GtkWindow *window;

  MAIL_ENTRY;

  g_assert (MAIL_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (MAIL_TYPE_WINDOW,
                           "application", self,
                           NULL);

  gtk_window_present (window);

  MAIL_EXIT;
}

static void
mail_application_startup (GApplication *app)
{
  MailApplication *self = (MailApplication *)app;

  MAIL_ENTRY;

  self->accounts = g_object_new (MAIL_TYPE_ACCOUNT_MANAGER, NULL);

  G_APPLICATION_CLASS (mail_application_parent_class)->startup (app);

  MAIL_EXIT;
}

static void
mail_application_shutdown (GApplication *app)
{
  MailApplication *self = (MailApplication *)app;

  MAIL_ENTRY;

  G_APPLICATION_CLASS (mail_application_parent_class)->shutdown (app);

  g_clear_object (&self->accounts);

  MAIL_EXIT;
}

static void
mail_application_class_init (MailApplicationClass *klass)
{
  GApplicationClass *g_app_class = G_APPLICATION_CLASS (klass);

  g_app_class->activate = mail_application_activate;
  g_app_class->startup = mail_application_startup;
  g_app_class->shutdown = mail_application_shutdown;
}

static void
mail_application_init (MailApplication *self)
{
}

MailApplication *
mail_application_new (void)
{
  return g_object_new (MAIL_TYPE_APPLICATION,
                       "application-id", "org.gnome.Mail",
                       NULL);
}
