/* mail-account-manager.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-account-manager"

#include "mail-account-manager.h"

struct _MailAccountManager
{
  GObject    parent_instance;
  GPtrArray *accounts;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (MailAccountManager, mail_account_manager, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static void
mail_account_manager_finalize (GObject *object)
{
  G_OBJECT_CLASS (mail_account_manager_parent_class)->finalize (object);
}

static void
mail_account_manager_class_init (MailAccountManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_account_manager_finalize;
}

static void
mail_account_manager_init (MailAccountManager *self)
{
}

static GType
mail_account_manager_get_item_type (GListModel *model)
{
  return MAIL_TYPE_ACCOUNT;
}

static guint
mail_account_manager_get_n_items (GListModel *model)
{
  return MAIL_ACCOUNT_MANAGER (model)->accounts->len;
}

static gpointer
mail_account_manager_get_item (GListModel *model,
                               guint       position)
{
  MailAccountManager *self = MAIL_ACCOUNT_MANAGER (model);

  return g_object_ref (g_ptr_array_index (self->accounts, position));
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = mail_account_manager_get_item_type;
  iface->get_n_items = mail_account_manager_get_n_items;
  iface->get_item = mail_account_manager_get_item;
}
