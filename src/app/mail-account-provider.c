/* mail-account-provider.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-account-provider"

#include "mail-account-provider.h"

G_DEFINE_INTERFACE (MailAccountProvider, mail_account_provider, G_TYPE_OBJECT)

enum {
  ADDED,
  REMOVED,
  N_SIGNALS
};

static guint signals [N_SIGNALS];

static void
mail_account_provider_default_init (MailAccountProviderInterface *iface)
{
  signals [ADDED] =
    g_signal_new ("added",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL,
                  G_TYPE_NONE, 1, MAIL_TYPE_ACCOUNT);

  signals [REMOVED] =
    g_signal_new ("removed",
                  G_TYPE_FROM_INTERFACE (iface),
                  G_SIGNAL_RUN_LAST,
                  0, NULL, NULL, NULL,
                  G_TYPE_NONE, 1, MAIL_TYPE_ACCOUNT);
}

void
mail_account_provider_emit_added (MailAccountProvider *self,
                                  MailAccount         *account)
{
  g_return_if_fail (MAIL_IS_ACCOUNT_PROVIDER (self));
  g_return_if_fail (MAIL_IS_ACCOUNT (account));

  g_signal_emit (self, signals [ADDED], 0, account);
}

void
mail_account_provider_emit_removed (MailAccountProvider *self,
                                    MailAccount         *account)
{
  g_return_if_fail (MAIL_IS_ACCOUNT_PROVIDER (self));
  g_return_if_fail (MAIL_IS_ACCOUNT (account));

  g_signal_emit (self, signals [REMOVED], 0, account);
}
