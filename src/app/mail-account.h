/* mail-account.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_ACCOUNT_H
#define MAIL_ACCOUNT_H

#include <gio/gio.h>

G_BEGIN_DECLS

#define MAIL_TYPE_ACCOUNT (mail_account_get_type())

G_DECLARE_DERIVABLE_TYPE (MailAccount, mail_account, MAIL, ACCOUNT, GObject)

struct _MailAccountClass
{
  GObjectClass parent_class;

  const gchar *(*get_id)         (MailAccount *self);
  const gchar *(*get_title)      (MailAccount *self);
  void         (*set_title)      (MailAccount *self,
                                  const gchar *title);
  gboolean     (*get_can_delete) (MailAccount          *self);
  void         (*delete_async)   (MailAccount          *self,
                                  GCancellable         *cancellable,
                                  GAsyncReadyCallback   callback,
                                  gpointer              user_data);
  gboolean     (*delete_finish)  (MailAccount          *self,
                                  GAsyncResult         *reuslt,
                                  GError              **error);

  gpointer _reserved1;
  gpointer _reserved2;
  gpointer _reserved3;
  gpointer _reserved4;
  gpointer _reserved5;
  gpointer _reserved6;
  gpointer _reserved7;
  gpointer _reserved8;
  gpointer _reserved9;
  gpointer _reserved10;
  gpointer _reserved11;
  gpointer _reserved12;
  gpointer _reserved13;
  gpointer _reserved14;
  gpointer _reserved15;
  gpointer _reserved16;
};

MailAccount *mail_account_new           (void);
const gchar *mail_account_get_id        (MailAccount          *self);
const gchar *mail_account_get_title     (MailAccount          *self);
void         mail_account_set_title     (MailAccount          *self,
                                         const gchar          *title);
void         mail_account_delete_async  (MailAccount          *self,
                                         GCancellable         *cancellable,
                                         GAsyncReadyCallback   callback,
                                         gpointer              user_data);
gboolean     mail_account_delete_finish (MailAccount          *self,
                                         GAsyncResult         *reuslt,
                                         GError              **error);

G_END_DECLS

#endif /* MAIL_ACCOUNT_H */
