/* mail-application.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_APPLICATION_H
#define MAIL_APPLICATION_H

#include <gtk/gtk.h>

#include "mail-account-manager.h"

G_BEGIN_DECLS

#define MAIL_TYPE_APPLICATION (mail_application_get_type())

G_DECLARE_FINAL_TYPE (MailApplication, mail_application, MAIL, APPLICATION, GtkApplication)

MailApplication    *mail_application_new          (void);
MailAccountManager *mail_application_get_accounts (MailApplication *self);

G_END_DECLS

#endif /* MAIL_APPLICATION_H */
