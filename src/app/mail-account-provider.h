/* mail-account-provider.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_ACCOUNT_PROVIDER_H
#define MAIL_ACCOUNT_PROVIDER_H

#include <gio/gio.h>

#include "mail-account.h"

G_BEGIN_DECLS

#define MAIL_TYPE_ACCOUNT_PROVIDER (mail_account_provider_get_type())

G_DECLARE_INTERFACE (MailAccountProvider, mail_account_provider, MAIL, ACCOUNT_PROVIDER, GObject)

struct _MailAccountProviderInterface
{
  GTypeInterface parent_interface;

  void     (*load_async)  (MailAccountProvider  *self,
                           GCancellable         *cancellable,
                           GAsyncReadyCallback   callback,
                           gpointer              user_data);
  gboolean (*load_finish) (MailAccountProvider  *self,
                           GAsyncResult         *result,
                           GError              **error);
};

void     mail_account_provider_emit_added   (MailAccountProvider  *self,
                                             MailAccount          *account);
void     mail_account_provider_emit_removed (MailAccountProvider  *self,
                                             MailAccount          *account);
void     mail_account_provider_load_async   (MailAccountProvider  *self,
                                             GCancellable         *cancellable,
                                             GAsyncReadyCallback   callback,
                                             gpointer              user_data);
gboolean mail_account_provider_load_finish  (MailAccountProvider  *self,
                                             GAsyncResult         *result,
                                             GError              **error);

G_END_DECLS

#endif /* MAIL_ACCOUNT_PROVIDER_H */
