/* mail-protocol-service.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_PROTOCOL_SERVICE_H
#define MAIL_PROTOCOL_SERVICE_H

#include <gio/gio.h>

#include "mail-protocol.h"

G_BEGIN_DECLS

#define MAIL_TYPE_PROTOCOL_SERVICE (mail_protocol_service_get_type())

G_DECLARE_FINAL_TYPE (MailProtocolService, mail_protocol_service, MAIL, PROTOCOL_SERVICE, GObject)

MailProtocolService *mail_protocol_service_new (MailProtocolType protocol);

G_END_DECLS

#endif /* MAIL_PROTOCOL_SERVICE_H */
