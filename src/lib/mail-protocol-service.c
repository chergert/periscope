/* mail-protocol-service.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-protocol-service"

#include "mail-enums.h"
#include "mail-protocol-service.h"

struct _MailProtocolService
{
  GObject          parent_instance;
  MailProtocolType protocol;
};

enum {
  PROP_0,
  PROP_PROTOCOL,
  N_PROPS
};

G_DEFINE_TYPE (MailProtocolService, mail_protocol_service, G_TYPE_OBJECT)

static GParamSpec *properties [N_PROPS];

static void
mail_protocol_service_finalize (GObject *object)
{
  G_OBJECT_CLASS (mail_protocol_service_parent_class)->finalize (object);
}

static void
mail_protocol_service_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  MailProtocolService *self = MAIL_PROTOCOL_SERVICE (object);

  switch (prop_id)
    {
    case PROP_PROTOCOL:
      g_value_set_enum (value, self->protocol);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_protocol_service_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  MailProtocolService *self = MAIL_PROTOCOL_SERVICE (object);

  switch (prop_id)
    {
    case PROP_PROTOCOL:
      self->protocol = g_value_get_enum (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_protocol_service_class_init (MailProtocolServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_protocol_service_finalize;
  object_class->get_property = mail_protocol_service_get_property;
  object_class->set_property = mail_protocol_service_set_property;

  properties [PROP_PROTOCOL] =
    g_param_spec_enum ("protocol",
                       "Protocol",
                       "Protocol",
                       MAIL_TYPE_PROTOCOL_TYPE,
                       MAIL_PROTOCOL_IMAP,
                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
mail_protocol_service_init (MailProtocolService *self)
{
}
