/* mail-password-credentials.c
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "mail-password-credentials"

#include "mail-credentials.h"
#include "mail-password-credentials.h"

typedef struct
{
  gchar *username;
  gchar *password;
} MailPasswordCredentialsPrivate;

enum {
  PROP_0,
  PROP_USERNAME,
  PROP_PASSWORD,
  N_PROPS
};

G_DEFINE_TYPE_WITH_CODE (MailPasswordCredentials, mail_password_credentials, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (MailPasswordCredentials)
                         G_IMPLEMENT_INTERFACE (MAIL_TYPE_CREDENTIALS, NULL))

static GParamSpec *properties [N_PROPS];

static void
mail_password_credentials_finalize (GObject *object)
{
  MailPasswordCredentials *self = (MailPasswordCredentials *)object;
  MailPasswordCredentialsPrivate *priv = mail_password_credentials_get_instance_private (self);

  g_clear_pointer (&priv->username, g_free);
  g_clear_pointer (&priv->password, g_free);

  G_OBJECT_CLASS (mail_password_credentials_parent_class)->finalize (object);
}

static void
mail_password_credentials_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  MailPasswordCredentials *self = MAIL_PASSWORD_CREDENTIALS (object);

  switch (prop_id)
    {
    case PROP_PASSWORD:
      g_value_set_string (value, mail_password_credentials_get_password (self));
      break;

    case PROP_USERNAME:
      g_value_set_string (value, mail_password_credentials_get_username (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_password_credentials_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  MailPasswordCredentials *self = MAIL_PASSWORD_CREDENTIALS (object);

  switch (prop_id)
    {
    case PROP_PASSWORD:
      mail_password_credentials_set_password (self, g_value_get_string (value));
      break;

    case PROP_USERNAME:
      mail_password_credentials_set_username (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mail_password_credentials_class_init (MailPasswordCredentialsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = mail_password_credentials_finalize;
  object_class->get_property = mail_password_credentials_get_property;
  object_class->set_property = mail_password_credentials_set_property;

  properties [PROP_PASSWORD] =
    g_param_spec_string ("password",
                         "Password",
                         "The password for the credentials",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  properties [PROP_USERNAME] =
    g_param_spec_string ("username",
                         "Username",
                         "The username for the credentials",
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
mail_password_credentials_init (MailPasswordCredentials *self)
{
}

const gchar *
mail_password_credentials_get_password (MailPasswordCredentials *self)
{
  MailPasswordCredentialsPrivate *priv = mail_password_credentials_get_instance_private (self);

  g_return_val_if_fail (MAIL_IS_PASSWORD_CREDENTIALS (self), NULL);

  return priv->password;
}

const gchar *
mail_password_credentials_get_username (MailPasswordCredentials *self)
{
  MailPasswordCredentialsPrivate *priv = mail_password_credentials_get_instance_private (self);

  g_return_val_if_fail (MAIL_IS_PASSWORD_CREDENTIALS (self), NULL);

  return priv->username;
}

void
mail_password_credentials_set_password (MailPasswordCredentials *self,
                                        const gchar             *password)
{
  MailPasswordCredentialsPrivate *priv = mail_password_credentials_get_instance_private (self);

  g_return_if_fail (MAIL_IS_PASSWORD_CREDENTIALS (self));

  if (g_strcmp0 (priv->password, password) != 0)
    {
      g_free (priv->password);
      priv->password = g_strdup (password);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PASSWORD]);
    }
}

void
mail_password_credentials_set_username (MailPasswordCredentials *self,
                                        const gchar             *username)
{
  MailPasswordCredentialsPrivate *priv = mail_password_credentials_get_instance_private (self);

  g_return_if_fail (MAIL_IS_PASSWORD_CREDENTIALS (self));

  if (g_strcmp0 (priv->username, username) != 0)
    {
      g_free (priv->username);
      priv->username = g_strdup (username);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_USERNAME]);
    }
}

MailCredentials *
mail_password_credentials_new (void)
{
  return g_object_new (MAIL_TYPE_PASSWORD_CREDENTIALS, NULL);
}
