/* mail-password-credentials.h
 *
 * Copyright (C) 2017 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIL_PASSWORD_CREDENTIALS_H
#define MAIL_PASSWORD_CREDENTIALS_H

#include <gio/gio.h>

G_BEGIN_DECLS

#define MAIL_TYPE_PASSWORD_CREDENTIALS (mail_password_credentials_get_type())

G_DECLARE_DERIVABLE_TYPE (MailPasswordCredentials, mail_password_credentials, MAIL, PASSWORD_CREDENTIALS, GObject)

struct _MailPasswordCredentialsClass
{
  GObjectClass parent_class;

  gpointer _reserved1;
  gpointer _reserved2;
  gpointer _reserved3;
  gpointer _reserved4;
};

MailCredentials *mail_password_credentials_new          (void);
const gchar     *mail_password_credentials_get_username (MailPasswordCredentials *self);
void             mail_password_credentials_set_username (MailPasswordCredentials *self,
                                                         const gchar             *username);
const gchar     *mail_password_credentials_get_password (MailPasswordCredentials *self);
void             mail_password_credentials_set_password (MailPasswordCredentials *self,
                                                         const gchar             *password);

G_END_DECLS

#endif /* MAIL_PASSWORD_CREDENTIALS_H */
